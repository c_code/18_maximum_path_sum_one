/* maximumPathSumOne
 *
 * Finds the maximum sum that can be found from traveling the triange from top to bottom 
 * only traveling adjacent numbers on the rows below
 *
 *eg:                   1
 *                     3 5
 *                    6 1 1
 *
 *                    Greatest sum is 10 by going 1->3->6
 *
 * Author: Jeremy Tobac
 * Date: October 2014
 */
#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 15
#define PRINT_TRIANGLE 0

/**@brief Read in the triangular aray data from the file numbers.txt
 *
 *@param int **ppintArray: The array to be filled with numbers
 *
 *@ret Returns 0 on success, -1 on failure
 */
int read_data(int **ppintArray)
{
	FILE *pfIn;
	int intRow, intColumn, intI;
	char achrTemp[5] = {0};
	
	intRow = intColumn = intI = 0;

	pfIn = fopen("numbers.txt", "r");
	if(pfIn == NULL){
		fprintf(stderr, "Error opening numbers.txt\n");
		return -1;
	}
	
	while((achrTemp[intI] = fgetc(pfIn)) != EOF){
		if(achrTemp[intI] == ' '){
			achrTemp[intI] = '\0';
			ppintArray[intRow][intColumn] = atoi(achrTemp);
			intI = 0;
			intColumn++;
		}
		else if(achrTemp[intI] == '\n'){
			achrTemp[intI] = '\0';
			ppintArray[intRow][intColumn] = atoi(achrTemp);
			intI = 0;
			intColumn = 0;
			intRow++;
		}
		else{
			intI++;
		}
	}

	if(fclose(pfIn)){
		/* not returning -1 because program can continue */
		fprintf(stderr, "Error closing input file\n");
	}
	
	return 0;
}

/**@brief Prints the triangular array of numbers
 *
 *@param int **ppintArray: The array to be filled with numbers
 *@param int aintRowSize[]: Array that says the number of columns in each row
 */
void print_triangle(int **ppintArray, int aintRowSize[])
{
	int intRow, intColumn;

	for(intRow = 0; intRow < NUM_ROWS; intRow++){
		for(intColumn = 0; intColumn < aintRowSize[intRow]; intColumn++){
			printf("%2d, ", ppintArray[intRow][intColumn]);
		}
		printf("\n");
	}
}

/**@brief Finds the maximum value from top to bottom traveling one line at a time along a triangle of numbers
 *
 *@param int **ppintArray: Array of numbers in the triangle
 *@param int aintRowSize[]: The maximum size of each row
 *@param int intRow: The total number of rows minus one in order to start from zero
 *
 *@ret Returns the sum of the numbers in the triangle when the greatest path is traveled from top to bottom
 */
int max_path_value(int **ppintArray, int aintRowSize[], int intRow)
{
	int intColumn, intMax;
	
	intRow--;
	intColumn = 0;

	for(;intRow >= 0;intRow--){
		for(intColumn = 0; intColumn < aintRowSize[intRow]; intColumn++){
			intMax = ppintArray[intRow+1][intColumn] > ppintArray[intRow+1][intColumn+1] ? ppintArray[intRow+1][intColumn] : ppintArray[intRow+1][intColumn+1];
			ppintArray[intRow][intColumn] = ppintArray[intRow][intColumn] + intMax;
		}
	}

	return ppintArray[0][0];

}

int main()
{

	int **ppintArray;
	int intI, intRet, intSum;
	int aintRowSize[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	intSum = 0;

	ppintArray = (int**)malloc(sizeof(int*) * NUM_ROWS);
	if(ppintArray == NULL){
		fprintf(stderr, "Error mallocing ppintArray\n");
		return -1;
	}

	for(intI = 0; intI < NUM_ROWS;intI++){
		ppintArray[intI] = (int*)malloc(sizeof(int)*aintRowSize[intI]);
		if(ppintArray[intI] == NULL){
			fprintf(stderr, "Error mallocing ppintArray[%d]\n", intI);
			return -1;
		}
	}

	intRet = read_data(ppintArray);
	if(intRet){
		return -1;
	}
#if PRINT_TRIANGLE
	print_triangle(ppintArray, aintRowSize);
#endif
	intSum = max_path_value(ppintArray, aintRowSize, ((int)NUM_ROWS - 1));
	printf("The value from top to bottom which gives the max sum is %d\n", intSum);

	/* free pointers */
	for(intI = 0; intI < NUM_ROWS; intI++){
		free(ppintArray[intI]);
		ppintArray[intI] = NULL;
		if(ppintArray[intI]){
			fprintf(stderr, "Error freeing pointer ppintArray[%d]\n", intI);
			return -1;
		}
	}

	free(ppintArray);
	ppintArray = NULL;
	if(ppintArray){
		fprintf(stderr, "Error freeing pointer ppintArray\n");
		return -1;
	}

	return 0;
}
